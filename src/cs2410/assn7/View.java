package cs2410.assn7;


import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.File;

/**
 * This class is a video viewer
 * @author Alex Isaac
 * @version 1.0
 */
public class View extends Application
{
    /**
     * Source file for video
     */
    private Media videoSource;

    /**
     * Media controller
     */
    private MediaPlayer video;

    /**
     * Video viewer
     */
    private MediaView videoPlayer;

    /**
     * File Chooser for getting file
     */
    private FileChooser fileChooser;

    /**
     * Slider for time
     */
    private Slider timeSldr;

    /**
     * is the video currently playing
     */
    boolean isPlaying = false;

    /**
     * if this is the first time a video has been loaded
     */
    boolean firstRun = true;


    /**
     * Main method for creating the application
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        //panes
        BorderPane rootPane = new BorderPane();
        Pane controlPane = new Pane();
        controlPane.setMaxHeight(100);
        rootPane.setBottom(controlPane);

        //time slider and label
        timeSldr = new Slider();
        Label timeLbl = new Label("Time:", timeSldr);
        timeLbl.setLayoutX(400);
        controlPane.getChildren().addAll(timeSldr, timeLbl);

        Pane videoPane = new Pane();
        videoPane.setPrefSize(400,400);
        //videoSource = new Media(new File("data/MostBeautifulGirl.mp4").toURI().toString());
        //video = new MediaPlayer(videoSource);

        //set up mediaview
        videoPlayer = new MediaView();
        videoPlayer.setMediaPlayer(video);
        videoPlayer.setFitWidth(600);
        videoPlayer.setX(0);
        videoPlayer.setY(0);

        //add mediaview
        videoPane.getChildren().add(videoPlayer);
        rootPane.setCenter(videoPane);

        //Play button
        Button playBtn = new Button();
        Image playImage = new Image("file:data/PlayBtn.png");
        ImageView playView = new ImageView(playImage);
        Image pauseImage = new Image("file:data/PauseBtn.png");
        ImageView pauseView = new ImageView(pauseImage);
        playBtn.setGraphic(playView);
        playBtn.setLayoutX(10);
        playBtn.setLayoutY(-10);
        playBtn.setMinWidth(80);
        playBtn.setMinHeight(40);

        //add the play button
        controlPane.getChildren().add(playBtn);

        //Stop Button
        Button stopBtn = new Button();
        Image stopImage = new Image("file:data/StopBtn.png");
        stopBtn.setGraphic(new ImageView(stopImage));
        stopBtn.setLayoutX(100);
        stopBtn.setLayoutY(-10);
        stopBtn.setMinHeight(40);
        stopBtn.setMinWidth(80);

        controlPane.getChildren().add(stopBtn);

        //volume slider and label
        Slider volumeSldr = new Slider(0, 1, 1);
        Label volumeLbl = new Label("Volume:", volumeSldr);
        volumeLbl.setContentDisplay(ContentDisplay.RIGHT);
        volumeLbl.setLayoutX(195);

        controlPane.getChildren().addAll(volumeSldr, volumeLbl);

        //menu items
        //close menu item
        MenuItem closeItm = new MenuItem("Close");

        //exit menu item
        MenuItem exitItm = new MenuItem("Exit");
        exitItm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                System.exit(0);
            }
        });

        //open item
        MenuItem openItm = new MenuItem("Open");
        openItm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                //get the file
                File file = fileChooser.showOpenDialog(primaryStage);

                //if the file isnt null
                if(file != null)
                {
                    //setup video source and player
                    videoSource = new Media(file.toURI().toString());
                    video = new MediaPlayer(videoSource);
                    videoPlayer.setMediaPlayer(video);

                    //wait until the video is loaded and then set up the time slider
                    video.setOnReady(new Runnable() {
                        @Override
                        public void run() {
                            timeSldr.setMax(video.getTotalDuration().toSeconds());
                            timeSldr.setValue(0);
                            System.out.println("Seconds: " + video.getTotalDuration().toSeconds());
                            timeLbl.setContentDisplay(ContentDisplay.RIGHT);
                            timeSldr.valueProperty().addListener(new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                                    video.seek(Duration.seconds(timeSldr.getValue()));
                                }
                            });
                        }
                    });

                    //if this is the first video loaded then setup all the event handlers
                    if(firstRun)
                    {
                        playBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event)
                            {
                                if(isPlaying)
                                {
                                    video.pause();
                                    playBtn.setGraphic(playView);
                                    isPlaying = false;
                                }
                                else
                                {
                                    video.play();
                                    playBtn.setGraphic(pauseView);
                                    isPlaying = true;
                                }
                            }
                        });
                        stopBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event)
                            {
                                video.stop();
                                isPlaying = false;
                                playBtn.setGraphic(playView);
                            }
                        });

                        volumeSldr.valueProperty().addListener(new ChangeListener<Number>() {
                            @Override
                            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
                            {
                                video.setVolume(volumeSldr.getValue());
                            }
                        });

                        closeItm.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                videoSource = null;
                                video.dispose();
                                videoPlayer.setMediaPlayer(null);
                            }
                        });

                        firstRun = false;
                    }
                }
            }
        });


        //file menu
        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(openItm, closeItm, exitItm);

        //Help menu and items
        MenuItem docItm = new MenuItem("Documentation");
        MenuItem aboutItm = new MenuItem("About");
        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().addAll(docItm, aboutItm);

        //menu bar
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, helpMenu);
        rootPane.setTop(menuBar);


        //fileChooser
        fileChooser = new FileChooser();
        fileChooser.setTitle("Select Video");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MP4", "*.mp4"),
                new FileChooser.ExtensionFilter("M4V", "*.m4v"),
                new FileChooser.ExtensionFilter("M4A", "*.m4a"));


        Scene scene = new Scene(rootPane, 600, 450);

        primaryStage.setScene(scene);

        primaryStage.show();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Did the bonus where graphics are put in for the play/pause and stop buttons");

        alert.showAndWait();


    }
}
